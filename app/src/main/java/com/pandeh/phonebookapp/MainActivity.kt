package com.pandeh.phonebookapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.pandeh.phonebookapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonForGroup.setOnClickListener {

            binding.group.visibility = View.VISIBLE
        }
    }
}