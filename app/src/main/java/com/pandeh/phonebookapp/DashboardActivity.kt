package com.pandeh.phonebookapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.button.MaterialButton
import com.pandeh.phonebookapp.databinding.ActivityDashboardBinding
import dagger.hilt.android.AndroidEntryPoint
import java.time.Duration

class DashboardActivity : AppCompatActivity() {

    private lateinit var bindign: ActivityDashboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindign = ActivityDashboardBinding.inflate(layoutInflater)

//        setContentView(R.layout.activity_dashboard)
        setContentView(bindign.root)


        print("On create")
        val buttonTodo: MaterialButton = findViewById(R.id.todoButton)

        buttonTodo.setOnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        val openIntentForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {

                if (it.resultCode == Activity.RESULT_OK)
                    showToast(it.data?.getStringExtra("message").toString())
                else
                    showToast("empty data")
            }

        bindign.editProfileButton.setOnClickListener {
            Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show()
            openIntentForResult.launch(
                Intent(this,IntentExampleActivity::class.java).apply {
                    putExtra("id",1)
                }
            )
        }

        bindign.profileButton.setOnClickListener {
            showToast("My profile")

            val intent = Intent(this, IntentExampleActivity::class.java)
            val mentorName = bindign.textViewMentorName.text.toString()
            intent.putExtra(Constants.MENTOR_NAME, mentorName)
            intent.putExtra("classNumber", 20)
            startActivity(intent)
        }

        bindign.layoutCards.setOnClickListener {

            val intent = Intent(this, ComponentActivity::class.java)
            startActivity(intent)
        }
        bindign.layoutCards.setOnClickListener {
            val intent = Intent(this,NavigationActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onStart() {
        super.onStart()
        print("on start")
    }

    override fun onResume() {
        super.onResume()
        print("on resume")
    }

    override fun onStop() {
        super.onStop()
        print("on stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        print("on destroy")
    }

    /*override fun onBackPressed() {
        super.onBackPressed()
        print("on back pressed!")
    }*/

    private fun print(msg: String){
        Log.i("activity state", "activity state : $msg")
    }


}