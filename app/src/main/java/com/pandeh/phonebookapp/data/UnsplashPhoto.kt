package com.pandeh.phonebookapp.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class UnsplashPhoto(
    val id: Int,
    val description: String,
    val user: UnsplashUser,
    val urls: Urls
) : Parcelable {

}