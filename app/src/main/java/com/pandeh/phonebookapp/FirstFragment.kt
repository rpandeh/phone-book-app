package com.pandeh.phonebookapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.pandeh.phonebookapp.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {


    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(layoutInflater, container, false)
        val view = binding.root
        // Inflate the layout for this fragment

        binding.textViewFragment1.setOnClickListener {
            val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment(200)
            Navigation.findNavController(view).navigate(action)
        }
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}