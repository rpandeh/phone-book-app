package com.pandeh.phonebookapp.ui.gallery

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.paging.LoadState
import co.fanavari.myapplication20.ui.gallery.UnsplashPhotoLoadStateAdapter
import com.google.android.material.snackbar.Snackbar
import com.pandeh.phonebookapp.R
import com.pandeh.phonebookapp.data.UnsplashPhoto
import com.pandeh.phonebookapp.databinding.FragmentGalleryBinding
import com.pandeh.phonebookapp.utils.ConnectivityManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery) {

    private var _binding: FragmentGalleryBinding? = null
    private val binding get() = _binding!!

    private val viewModel by viewModels<GalleryViewModel>()

    @Inject
    lateinit var connectivityManager: ConnectivityManager
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentGalleryBinding.bind(view)

        connectivityManager.registerConnectionObserver(this)
        val adapter = UnsplashPhotoAdapter()
        binding.apply {

            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter
                .withLoadStateHeaderAndFooter(
                    header = UnsplashPhotoLoadStateAdapter { adapter.retry() },
                    footer = UnsplashPhotoLoadStateAdapter { adapter.retry() }
                )
            buttonRetry.setOnClickListener {
                adapter.retry()
            }
        }

        adapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                recyclerView.isVisible = loadState.source.refresh is LoadState.NotLoading
                buttonRetry.isVisible = loadState.source.refresh is LoadState.Error
                textViewError.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached &&
                    adapter.itemCount < 1
                ) {
                    recyclerView.isVisible = false
                    textViewEmptyList.isVisible = true
                } else {
                    textViewEmptyList.isVisible = false
                }
            }

        }

        viewModel.photos.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        connectivityManager.isNetWorkAvailable.observe(viewLifecycleOwner) {
            if (!it)
                Snackbar.make(binding.root, "no Internet", Snackbar.LENGTH_SHORT)
                    .show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        connectivityManager.unRegisterConnectionObserver(this)
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        connectivityManager.registerConnectionObserver(this)
    }
}