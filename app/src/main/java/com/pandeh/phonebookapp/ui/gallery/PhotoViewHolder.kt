package com.pandeh.phonebookapp.ui.gallery

import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.pandeh.phonebookapp.data.UnsplashPhoto
import com.pandeh.phonebookapp.databinding.ItemUnsplashBinding

class PhotoViewHolder(private val binding: ItemUnsplashBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(photo: UnsplashPhoto) {
        binding.apply {
            titleAnimal.text = photo.user.userName
            Glide.with(itemView)
                .load(photo.urls.regular)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(animalImageView)
        }
    }
}
