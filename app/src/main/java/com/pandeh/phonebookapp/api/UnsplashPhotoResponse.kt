package com.pandeh.phonebookapp.api

import com.pandeh.phonebookapp.data.UnsplashPhoto

data class UnsplashPhotoResponse(
    val results: List<UnsplashPhoto>
) {

}
